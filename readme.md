## Docker con WP + PHP + Apache + WP-CLI + Mysql + PMA + SSL


### Instalación / configuración

- La plantilla está configurada para crear certificados SSL a través de mkcert, para poder tener nuestro dominio bajo SSL, intrucciones para instalarlo: https://github.com/FiloSottile/mkcert
- Se puede elegir la versión de WordPress y de PHP cambiando la primera sentencia del `Dockerfile` eligiendo cualquiera de las imágenes oficiales disponibles en https://hub.docker.com/_/wordpress/
- El resto de contenedores/servicios están indicados desde el archivo `docker-compose.yml`
- Xdebug queda instalado en el servicio `wp` que es el que contiene WordPress, PHP y Apache.
- Modificar las variables de entorno del archivo `.env` por las de nuestro proyecto particular.
- Modificar archivo `000-default.conf`, indicando el nombre correcto de los certificados que hayamos creado previamente



#### Archivos personalizados

- `.htaccess`
- `000-default.conf`
- `php.conf.ini`
- `xdebug.ini`



### Puesta en marcha

1. ```git clone git@bitbucket.org:flat101team/docker-compose-wordpress-php-apache-pma.git```
2. Modificamos las variables de entorno del archivo `.env`, especialmente `APP_NAME`
3. Si queremos cambiar la versión de WordPress, podremos hacerlo modificando la línea `FROM` del archivo `Dockerfile`
4. **En el caso de que no queramos tener Redis en el proyecto, bastará con comentar las líneas correspondientes a dicho servicio en el archivo `docker-compose.yml`.**
5. ```cd docker/config/```
6. ```mkdir ssl && cd ssl```
7. `mkcert -install nombre-dominio-proyecto`
8. Modificamos el archivo `000-default.conf`, indicando el nombre correcto de SSLCertificateFile y SSLCertificateKeyFile, que corresponderán a los nombres de los certificados que hayamos creado previamente con mkcert.
9. `cd ../../..`
10. `docker-compose up -d`
11. Esperaremos unos instantes a que se copien los archivos de WordPress para que el servidor pueda responder.
12. Realizamos la configuración inicial de WordPress
13. Si queremos tener Redis activado, instalamos el plugin para Redis con wp-cli: `docker-compose run --rm wpcli plugin install redis-cache`
14. Activamos el plugin de Redis y comprobamos la conexión


### WP-CLI

Para ejecutar wp-cli: `docker-compose run --rm wpcli {comando}`

Nos muestra la info del contenedor de wp-cli: `docker-compose run --rm wpcli`

Para mostrar los usuarios de WP: `docker-compose run --rm wpcli user list`



